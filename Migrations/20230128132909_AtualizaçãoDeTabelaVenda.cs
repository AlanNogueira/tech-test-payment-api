﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace techtestpaymentapi.Migrations
{
    /// <inheritdoc />
    public partial class AtualizaçãoDeTabelaVenda : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Vendas_Vendedores_vendedorId",
                table: "Vendas");

            migrationBuilder.DropIndex(
                name: "IX_Vendas_vendedorId",
                table: "Vendas");

            migrationBuilder.AlterColumn<int>(
                name: "vendedorId",
                table: "Vendas",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "vendedorId",
                table: "Vendas",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.CreateIndex(
                name: "IX_Vendas_vendedorId",
                table: "Vendas",
                column: "vendedorId");

            migrationBuilder.AddForeignKey(
                name: "FK_Vendas_Vendedores_vendedorId",
                table: "Vendas",
                column: "vendedorId",
                principalTable: "Vendedores",
                principalColumn: "Id");
        }
    }
}
