using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tech_test_payment_api.Enums;

namespace tech_test_payment_api.Entities
{
	public class Venda : BaseEntity
	{
		public int vendedorId { get; set; }
		
		public DateTime Date { get; set; }
		
		public string items { get; set; }
		
		public VendaStatus status { get; set; }
	}
}