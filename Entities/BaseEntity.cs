using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace tech_test_payment_api.Entities
{
	public class BaseEntity
	{
		public int Id { get; set; }
		
		[JsonIgnore]
		public DateTime? DeletionDate { get; set; }
	}
}