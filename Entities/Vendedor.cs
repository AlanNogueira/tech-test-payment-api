using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.Entities
{
	public class Vendedor : BaseEntity
	{

		public string cpf { get; set; }

		public string Name { get; set;}

		public string mail { get; set; }
	
		public string phone { get; set; }
	}
}