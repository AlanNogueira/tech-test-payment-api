namespace tech_test_payment_api.Enums
{
	public enum VendaStatus
	{
		AguardandoPagamento = 0,
		PagamentoAprovado = 1,
		EnviadoParaATransportadora = 2,
		Entregue = 3,
		Cancelada = 4
	}
}