using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Entities;
using tech_test_payment_api.Enums;

namespace tech_test_payment_api.Controllers
{
	[ApiController]
	[Route("[Controller]")]
	public class PaymentController : ControllerBase
	{
		private readonly PaymentContext _context;
		
		public PaymentController(PaymentContext context)
		{
			_context = context;
		}
		
		
		[HttpPost("RegistrarVenda")]
		public IActionResult RegistrarVenda(Vendedor vendedor, string items)
		{
			if(String.IsNullOrEmpty(items))
				return BadRequest("A venda necessita ter pelo menos um item");
				
			string item = items.Replace(" ", "");
			Vendedor vendedorExistente = _context.Vendedores.Find(vendedor.Id);
			
			List<string> listaItems = item.Split(",").ToList();
			
			if(vendedorExistente == null)
				return BadRequest("Este vendedor não foi encontrado");
				
			if(vendedor.cpf != vendedorExistente.cpf && vendedor.Name != vendedorExistente.Name)
				return BadRequest("Dados do vendedor estão incorretos");
				
			if(listaItems.Count() == 0)
				return BadRequest("A venda necessita ter pelo menos um item");
			
			Venda venda = new Venda
			{
				vendedorId = vendedor.Id,
				items = items,
				status = VendaStatus.AguardandoPagamento,
				Date = DateTime.Now,
			};
			
			_context.Add(venda);
			_context.SaveChanges();
			return Ok(venda);
		}
		
		[HttpPost("CriarVendedor")]
		public IActionResult CriarVendedor(Vendedor vendedor)
		{
			Vendedor vendedorExistente = _context.Vendedores.Find(vendedor.Id);
			
			if(vendedorExistente != null)
				return BadRequest("Este vendedor já foi cadastrado");
			
			_context.Add(vendedor);
			_context.SaveChanges();
			return Ok(vendedor);
		}
		
		[HttpGet("BuscarVendaPorId/{Id}")]
		public IActionResult BuscarVendaPorId(int Id)
		{
			Venda venda = _context.Vendas.Find(Id);
			
			if(venda == null)
				return BadRequest("Venda não encontrada");
			
			return Ok(venda);
		}
		
		[HttpPut("AtualizarCompra/{Id}")]
		public IActionResult AtualizarCompra(int Id, VendaStatus vendaStatus)
		{
			Venda venda = _context.Vendas.Find(Id);
			
			if(venda == null)
				return BadRequest("Venda não encontrada");
				
			if(venda.status == VendaStatus.AguardandoPagamento)
			{
				if(vendaStatus != VendaStatus.PagamentoAprovado && vendaStatus != VendaStatus.Cancelada)
					return BadRequest("Uma compra com status de Aguardando pagamento só pode ser alterado para Pagamento Aprovado ou para Cancelado.");
			}
			
			if(venda.status == VendaStatus.PagamentoAprovado)
			{
				if(vendaStatus != VendaStatus.EnviadoParaATransportadora && vendaStatus != VendaStatus.Cancelada)
					return BadRequest("Uma compra com status de Pagamento Aprovado só pode ser alterado para Enviado Para a Transportadora ou para Cancelado.");
			}
			
			if(venda.status == VendaStatus.EnviadoParaATransportadora)
			{
				if(vendaStatus != VendaStatus.Entregue)
					return BadRequest("Uma compra com status de Enviado Para a Transportadora só pode ser alterado para Entregue.");
			}
			
			venda.status = vendaStatus;
			
			_context.Vendas.Update(venda);
			_context.SaveChanges();
			
			return Ok(venda);
		}
	}
}